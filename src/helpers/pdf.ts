import {
  PDFDocument,
} from 'pdf-lib'
import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import Download from 'downloadjs'
import { DateTime } from "luxon";

export class Pdf {
  constructor(data:any) {
    this.data = data
  }

  data: {
    // header: []
    columns: []
    body: []
  }
  
  pdfDoc!: PDFDocument

  async generateAndDownload() {
    await this.loadTemplate()
    await this.addTable()
    await this.download()
  }

  // Converting between strings and ArrayBuffers
  // https://stackoverflow.com/a/11058858/3804973
  stringToArrayBuffer(str:any) {
    var buf = new ArrayBuffer(str.length * 2);
    var bufView = new Uint16Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
      bufView[i] = str.charCodeAt(i);
    }
    return buf;
  }

  async loadTemplate() {
    const pdfRequest = await fetch('/data/offer-template.pdf')
    const pdfData = await pdfRequest.arrayBuffer()

    this.pdfDoc = await PDFDocument.load(pdfData)
  }

  async addTable() {
    const table = new jsPDF({
      // orientation: "landscape",
      unit: "mm",
      format: [215.9, 279.4]
    })

    autoTable(table, {
      // head: [this.data.header],
      columns: this.data.columns,
      body: this.data.body,
      margin: {
        top: 8,
        left: 4,
        right: 4
      },
      styles: {
        fontSize: 11
      },
      headStyles: {
        fillColor: '#eb0045',
        lineColor: '#ffffff',
        lineWidth: 0.5,
        halign: 'center',
        fontSize: 11
      },
      // DODO: Get from column meta data
      columnStyles: {
        0: {halign: 'right'},
        2: {halign: 'right'},
        3: {halign: 'right', minCellWidth: 20},
        4: {halign: 'right', minCellWidth: 20},
        5: {halign: 'right', minCellWidth: 25}
      }
    })

    const tableArrayBuffer = table.output('arraybuffer')

    const tableDoc = await PDFDocument.load(tableArrayBuffer)

    // DODO: Check for number of pages and get and insert them all
    const [tablePage] = await this.pdfDoc.copyPages(tableDoc, [0])
    this.pdfDoc.insertPage(1, tablePage)
  }

  async download() {
    const pdfBytes = await this.pdfDoc.save()
    let dt = DateTime.now()
    let filename = 'Angebot-' + dt.toISODate() + '-um-' + dt.toFormat('HH-mm') + '.pdf'
    Download(pdfBytes, filename, 'application/pdf')
  }
}

// Default helper to automate the whole process with just one call
export default async (data:any) => {
  let pdf = new Pdf(data);
  await pdf.generateAndDownload()
}
