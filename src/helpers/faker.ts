import { random } from '@/helpers/toolbelt'

const fakerData = {
  product: {
    superlatives: [
      'Hyper',
      'Turbo',
      'Mega',
      'Über',
      'Immer'
    ],
    adjectives: [
      'Trockene',
      'Hygienische',
      'Saubere',
      'Strapazierbare'
    ],
    fillers:[
      ' Catering',
      ' Bauarbeiter',
      ' Straßenbau',
      ' Sicherheits',
      ' Abriß',
      ' Business',
      ' Straßenbau',
      ' Gastro',
      ' Presslufthammer',
      ' Leucht',
      ' Warn',
      ' Schutz',
      ' Sprengmeister',
      ' Stahl'
    ],
    objects: [
      'jacke',
      'hose',
      'schuhe',
      'uniform',
      'gesichtsmaske',
      'helmkombi',
      'weste',
      'montur'
    ],
    additions: [
      ' mit Extrataschen',
      ' mit Seitenschneiderhalterung',
      ' ohne sichtbare Nähte',
      ' mit eingearbeiteten Regenbogenstickmustern',
      ' mit Kaffeekannenhaken',
      ' mit Presslufthammerschlauchhalterung'
    ]
  } as any
}

export default (which:string) => {
  switch (which) {
    case 'product': {
      let name = ''
      let terms = ['superlatives', 'adjectives', 'fillers', 'objects', 'additions']
      let firstSet = false

      terms.forEach((term) => {
        let set = fakerData.product[term]
        
        if ((term == 'superlatives' || term == 'additions')) {
          if (random(100) < 33) {
            firstSet = true
            name += set[random(set.length)]
          }
        } else {
          if (firstSet && term != 'fillers') {
            name += set[random(set.length)].toLowerCase()
          } else {
            name += set[random(set.length)]
          }
        }
      })

      return name
    }
  }
}
