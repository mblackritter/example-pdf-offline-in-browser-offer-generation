export const random = (max:number, min:number = 0) => min + Math.floor(Math.random() * (max - min))

export const formatCurrency = (i18n:any, number:number) => 
  Intl.NumberFormat(
    i18n.locale,
    i18n.numberFormat
  ).format(number)