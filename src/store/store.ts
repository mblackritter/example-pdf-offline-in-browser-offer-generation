import { createStore, createLogger } from 'vuex'

const debug = process.env.NODE_ENV !== 'production'
// const debug = false

export default createStore({
  // modules: {
  //   base: base,
  //   session: session,
  //   products: products,
  //   offer: offer
  // },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
