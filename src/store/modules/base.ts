import { Module, VuexModule, getModule, Mutation, Action } from 'vuex-module-decorators'
import vuexStore from '@/store/store'
import store from '@/store'

import { random } from '@/helpers/toolbelt'
import faker from '@/helpers/faker'

import offerData from '@/data/offer-data.json'

@Module({namespaced: true, dynamic: true, store: vuexStore, name: 'base'})
export default class base extends VuexModule {
  config = {
    headers: {
      products: [],
      offer: []
    } as any
  }
  
  @Action
  async initialize() {
    // Make our life simple...
    // Thanks to `store` it's now even perfectly simple and we don't even neet this shorthand.
    // const {commit, rootState: state} = this.context

    // Session
    if (localStorage.getItem('isLoggedIn')) {
      store.session.setLogin(true)

      if (localStorage.getItem('offer')) {
        store.offer.set(JSON.parse(localStorage.getItem('offer') as string))
      }
    }

    // Config
    // In the future, when fetching this data separate of built app
    // fetch('/data/offer-data.json')
    //   .then(res => res.json())
    //   .then(data => commit('setConfig', data.config))
    store.setConfig(offerData.config)

    // Products
    // fetch('/data/offer-data-products.json')
    //   .then(res => res.json())
    //   .then(data => context.commit('setProducts', data))

    let productsData = []
    for (var i = 1; i <= 50; i++) {
      productsData.push({
        sku: i,
        product: faker('product'),
        price: Math.floor(Math.random() * 300)
      })
    }

    store.products.set(productsData)

    // Offer
    if (!store.offer.items[0]) {
      let offerItemsData = []

      for (var i = 1; i <= 10; i++) {
        let item = productsData[random(productsData.length)]
        let amount = random(100)
        let discountPercentage = random(25)
        let priceDiscounted = (item.price - (item.price / 100 * discountPercentage))

        offerItemsData.push({
          ...item,
          amount: amount,
          discountPercentage: discountPercentage,
          priceDiscounted: priceDiscounted,        
          total: amount * priceDiscounted
        })
      }
      store.offer.set(offerItemsData)
    }
  }

  @Mutation
  setConfig(config:any) {
    this.config = config
  }
}
