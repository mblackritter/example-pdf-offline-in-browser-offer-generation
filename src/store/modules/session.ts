import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import vuexStore from '@/store/store'
import store from '@/store'
import sha3 from 'crypto-js/sha3';

@Module({namespaced: true, dynamic: true, store: vuexStore, name: 'session'})
export default class session extends VuexModule {
  isLoggedIn = false

  i18n = {
    locale: 'de-DE',
    numberFormat: {
      style: 'currency',
      currency: 'EUR'
    }
  }

  modal = {
    amount: ' ',
    discountPercentage: 0,
    priceDiscounted: 0,
  } as any

  @Action
  login(data:any) {
    const logins = [
      {
        email: 'ritter@zypo.com',
        password: '1e2e9fc2002b002d75198b7503210c05a1baac4560916a3c6d93bcce3a50d7f00fd395bf1647b9abb8d1afcc9c76c289b0c9383ba386a956da4b38934417789e'
      },
      {
        email: 'demo',
        password: '97b1f83d4d7bb65a69d4fefd6fbe5a2e51d44ec1a954a44210d780e51f8fb8e45182ee319765d9d836fbb5081b9b58f4d701a84d150277c8ad84b533de929a02'
      }
    ]
    return new Promise<string>((resolve, reject) => {
      setTimeout(() => {
        let login:any = logins.find(login => login.email == data.email)

        if (login.password == sha3(data.password)) {
          this.setLogin(true)
          resolve('')
        } else {
          resolve('Ungültiger Login')
        }
      }, 500)
    })
  }

  @Action
  logout() {
    this.setLogin(false)
  }

  @Mutation
  setLogin(value:any) {
    if (value) {
      localStorage.setItem('isLoggedIn', value)
    } else {
      localStorage.removeItem('isLoggedIn')
    }

    this.isLoggedIn = value
  }

  @Mutation
  setModal(params:any) {
    params.init ? 
      this.modal = params.data
      :
      this.modal = Object.assign(
        {},
        this.modal,
        params.data
      )
  }
}