import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import vuexStore from '@/store/store'

import store from '@/store'

@Module({namespaced: true, dynamic: true, store: vuexStore, name: 'offer'})
export default class offer extends VuexModule {
  items = [] as any

  // Creates a new instance for AG-Grid to notice changes.
  // DODO: Send specific updates to AG-Grid.
  get clone() {
    return this.items.map((item:any) => item)
  }

  @Action
  addFromModal() {
    let item = store.session.modal
    this.add({...item, total: item.amount * item.priceDiscounted})
    
    localStorage.setItem('offer', JSON.stringify(this.items))
  }

  @Mutation
  set(items:any) {
    this.items = items
    localStorage.setItem('offer', JSON.stringify(this.items))
  }

  @Mutation
  add(item:any) {
    this.items.push(item)
  }

  // DODO: Make all this (add/delete) ID based, as we might've the same products and thereby SKUs
  //       multiple times with various configurations.
  @Mutation
  delete(sku:any) {
    this.items.splice(this.items.findIndex((item:any) => item.sku == sku), 1)
    localStorage.setItem('offer', JSON.stringify(this.items))
  }
}