import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import vuexStore from '@/store/store'

@Module({namespaced: true, dynamic: true, store: vuexStore, name: 'products'})
export default class products extends VuexModule {
  items = []

  // Creates a new instance for AG-Grid to notice changes.
  // DODO: Send specific updates.
  get clone() {
    return this.items.map((item:any) => item)
  }

  get item() {
    return (sku:any) => this.items.find((item:any) => item.sku === sku)
  }

  @Mutation
  set(items:any) {
    this.items = items
  }
}