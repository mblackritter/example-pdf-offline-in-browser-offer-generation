import { getModule } from 'vuex-module-decorators'
import _base from '@/store/modules/base'
import _session from '@/store/modules/session'
import _products from '@/store/modules/products'
import _offer from '@/store/modules/offer'

// Make modules available in our store root
const base = getModule(_base)
base.session = getModule(_session)
base.products = getModule(_products)
base.offer = getModule(_offer)

export default base
