import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from 'vite-plugin-pwa'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({
      manifest: {
        // content of manifest
      },
      workbox: {
        // workbox options for generateSW
      }
    })
  ],
  resolve: {
    alias: {
      '@': require('path').resolve(__dirname, 'src')
    }
  },
  server: {
    host: '0.0.0.0',
    port: 7777,
    // https: true
  }
})
