# Example - PDF Offline in Browser Offer Generation

## Usage

### Start Vite development server

`yarn dev`


### Build package into dist

`yarn vite build`



## Overview

- [Dev Stack Notes](#dev-stack-notes)
  - [Vue Script Setup](#vue-script-setup)
  - [Volt Template Integration](#volt-template-integration)

- [Packages](#used-packages)
  - [Framework](#framework)
  - [Build Setup](#build-setup)
  - [PDF Generation](#pdf-generation)
  - [HTML, CSS, Theming, Templating](#html-css-theming-templating)
  - [Store, State Machine](#store-state-machine)
    - [Store ORM](#store-orm)
  - [Data Grid](#data-grid)
  - [Localization](#localization)
  - [Form Validation](#form-validation)
  - [PWA - Progressive Web App](#pwa---progressive-web-app))
  - [Helpers](#helpers)
  - [Peer Dependencies](#peer-dependencies)


## Dev Stack Notes

### Vue Script Setup

We're using the new/upcoming `Script Setup` instead of `Setup()` function.

- [rfcs/active-rfcs/0000-script-setup.md](https://github.com/vuejs/rfcs/blob/master/active-rfcs/0040-script-setup.md)

- [Feature: Vue 3 script setup](https://dev.to/ghalex/feature-vue-3-script-setup-2ble)

- [Explaining The New script setup Type in Vue 3 – Major Takeaways from the RFC](https://learnvue.co/2021/05/explaining-the-new-script-setup-type-in-vue-3-major-takeaways-from-the-rfc/)


### Volt Template Integration



## Package Overview

### Framework

  - **Vue 3** ([GitHub](https://github.com/vuejs/vue-next), [Web](https://v3.vuejs.org))



### Build Setup  

  - **Vite** ([GitHub](https://github.com/vitejs/vite), [Web](https://vitejs.dev))



### PDF Generation

  - **jsPDF** ([GitHub](https://github.com/MrRio/jsPDF), [Docs](http://raw.githack.com/MrRio/jsPDF/master/docs/))

  - **jsPDF Autotable** ([GitHub](https://github.com/simonbengtsson/jsPDF-AutoTable), [Example](https://simonbengtsson.github.io/jsPDF-AutoTable/))

  - **PDF-Lib** ([GitHub](https://github.com/Hopding/pdf-lib), [Web](https://pdf-lib.js.org))

  - **SVG2PDF.js** ([GitHub](https://github.com/yWorks/svg2pdf.js), [Playground](http://raw.githack.com/yWorks/svg2pdf.js/master/))



### HTML, CSS, Theming, Templating

  - **Volt** ([GitHub](https://github.com/themesberg/volt-bootstrap-5-dashboard), [Web](https://demo.themesberg.com/volt/))



### Store, State Machine

  - **Vuex** ([GitHub](https://github.com/vuejs/vuex), [Web](https://vuex.vuejs.org))

<!--   - **Vuexok** ([GitHub](https://github.com/spb-web/vuexok), [Docs](https://spb-web.github.io/vuexok/))

    If compatible with Vue 3, else **Vuex Module Decorators**. -->

  - **Vuex Module Decorators** ([GitHub](https://github.com/championswimmer/vuex-module-decorators), [Web](https://championswimmer.in/vuex-module-decorators/)), for automatic typing of the Vuex modules
  
<!--
  2021-07-05: In case we should hit more complexity than expected
-->

<!--
  2021-07-06: Well, just finally had a great one with Jordy, and yeah, we've differing classes
  of Products/Offers, so there we go with polymorphism, or as they call it:
  [Single Table Inheritance](https://vuex-orm.org/guide/model/single-table-inheritance.html#inheritance-conventions).
-->

  - **Store ORM** <a name="store-orm"></a>
    
    Especially for [Polymorphism/Single Table Inheritance](https://vuex-orm.org/guide/model/single-table-inheritance.html#inheritance-conventions) to cover product types/classes.

    - **Vuex ORM** ([GitHub](https://github.com/vuex-orm/vuex-orm), [Web](https://vuex-orm.org))

    - **Vuex ORM Plugin - Search** ([GitHub](https://github.com/vuex-orm/plugin-search))

    - **Vuex ORM Plugin - IsDirty/IsNew Plugin** ([GitHub](https://github.com/vuex-orm/plugin-change-flags))
  


### Data Grid
  
  - **AG Grid** ([GitHub](https://github.com/ag-grid/ag-grid), [Web](https://www.ag-grid.com))

  - **AG Grid Vue 3** ([GitHub](https://github.com/ag-grid/ag-grid/tree/latest/grid-packages/ag-grid-vue3), [Web](https://www.ag-grid.com/vue-grid/vue3/))



### Localization
  
  - **Vue I18n** ([GitHub](https://github.com/kazupon/vue-i18n), [Web](https://kazupon.github.io/vue-i18n/))

  - **Vite Plugin Vue I18n** ([GitHub](https://github.com/intlify/vite-plugin-vue-i18n))



### Form Validation
  
  - **Vee-Validate** ([GitHub](https://github.com/logaretm/vee-validate), [Web](https://vee-validate.logaretm.com/v4/))



### PWA - Progressive Web App
  
  - **Zero Config PWA for Vite** ([GitHub](https://github.com/antfu/vite-plugin-pwa))



### Helpers
  
  - **PnPjs** ([GitHub](https://github.com/pnp/pnpjs), [Web](https://pnp.github.io/pnpjs/))

    Fluent JavaScript API for SharePoint and Microsoft Graph REST APIs


  - **Vue Axios** ([GitHub](https://github.com/imcvampire/vue-axios))

    A small wrapper for integrating [Axios](https://github.com/axios/axios) to Vue.

    > Might be a better choice to fetch files than JS's plain fetch().


  - **CryptoJS** ([GitHub](https://github.com/brix/crypto-js))

    Used temporarily for "fake" login.

  
  - **Luxon** ([GitHub](https://github.com/moment/luxon), [Web](https://moment.github.io/luxon/))
  
  - **DownloadJS** ([GitHub](https://github.com/rndme/download)

  - **FileSaver.js** ([GitHub](https://github.com/eligrey/FileSaver.js), [Web](https://eligrey.com/blog/saving-generated-files-on-the-client-side/))

    > More modern replacement for DownloadJS.


  - **JSZip** ([GitHub](https://github.com/Stuk/jszip), [Web](https://stuk.github.io/jszip/))


### Peer Dependencies
  
  - **Vue Class Component** ([GitHub](https://github.com/vuejs/vue-class-component), [Web](https://class-component.vuejs.org))
    
    Used by [AG Grid Vue 3](#data-grid)
